package basic
import processing.core._

class MyPApplet extends PApplet {
// The statements in the setup() function 
  // execute once when the program begins
  def _size(width : Int, height : Int) = {
    size(width, height)
  }
}