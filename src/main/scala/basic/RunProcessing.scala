package basic
import processing.core._

class RunProcessing extends PApplet {
  override def setup() = {
    size(640, 360)// Size must be the first statement
    stroke(255, 2, 55) // Set line drawing color to white
    frameRate(30)
  }
  override def draw() = {
    rect(0, 0, 100, 100)
  }
}