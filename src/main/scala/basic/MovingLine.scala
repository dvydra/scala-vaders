package basic
import processing.core._

class MovingLine extends MyPApplet {
  var _y = 100

  
  override def setup() {
    _size(width = 680, height = 260) // Size must be the first statement
    stroke(255); // Set line drawing color to white
    frameRate(30);
  }
  // The statements in draw() are executed until the 
  // program is stopped. Each statement is executed in 
  // sequence and after the last line is read, the first 
  // line is executed again.
  override def draw() {
    background(0); // Clear the screen with a black background
    _y = _y - 1;
    if (_y < 0) {
      _y = height;
    }
    line(0, _y, width, _y);
  }
}