package basic

import processing.core._

class LoadImage extends MyPApplet {
  
	var image : PImage = null 
	
  override def setup() = {
    image = loadImage("face.jpg")
  }
  

}